#+TITLE: Bspwmrc config
#+AUTHOR: Fet64
#+property: header-args :mkdirp yes :result silent :tangle ~/.config/bspwm/bspwmrc :shebang "#!/bin/sh"

* Table Of Contents :toc:
- [[#about-this-config][About this config]]
- [[#run-sxhkd][Run sxhkd]]
- [[#monitor-setup][Monitor setup]]
  - [[#multi-monitor-setups][Multi-monitor setups]]
- [[#config][Config]]
- [[#rules][Rules]]
- [[#autostart][Autostart]]
- [[#xsetroot][Xsetroot]]
- [[#battery-notifier][Battery notifier]]

* About this config
This org file contains the configuration file of bspwm. Bspwm is a binary space partitioning window manager, for more information: [[https://github.com/baskerville/bspwm]]. The files are tgangled from this file using =org-babel=. Output file is ~/.config/bspwm/bspwmrc.

View this file with Emacs in org-mode.

To create bspwmrc config file run M-x org-babel-tangle (C-c C-v t).

* Run sxhkd
sxhkd is an X daemon that reacts to input events by executing commands. [[(https://github.com/baskerville/sxhkd)]]

#+begin_src sh
pgrep -x sxhkd > /dev/null || sxhkd &

#+end_src

* Monitor setup
This code block configures ten desktops on one monitor.

#+begin_src sh

bspc monitor -d I II III IV V VI VII VIII IX X
#+end_src

** Multi-monitor setups
You will need to change the line above and add one for each monitor.
#+begin_src sh :tangle no

bspc monitor DVI-I-1 -d I II III IV
bspc monitor DVI-I-2 -d V VI VII
bspc monitor DP-1    -d VIII IX X
#+end_src

* Config
bspc config command sets or gets values.

#+begin_src sh
bspc config border_width         2
bspc config window_gap          12

bspc config split_ratio          0.52
bspc config borderless_monocle   true
bspc config gapless_monocle      true
bspc config pointer_follows_focus true
bspc config focus_follows_pointer true

# Border
bspc config focused_border_color        "#6c71c4"
bspc config normal_border_color         "#073642"
bspc config active_border_color         "#073642"
#+end_src

* Rules
bspc rule sets rules for how different programs should behave. To find your programs s you cant use xprop tool and look for WM_CLASS(STRING) = "unimportant", "NAME_TO_USE". If you run xprop on a firefox window you will get: M_CLASS(STRING) = "Navigator", "firefox".

#+begin_src sh
bspc rule -a Gimp desktop='^8' state=floating follow=on
bspc rule -a Thunar desktop=III
bspc rule -a Emacs desktop=IV state=fullscreen
bspc rule -a firefox desktop='^2'
bspc rule -a mplayer2 state=floating
bspc rule -a Kupfer.py focus=on
bspc rule -a Screenkey manage=off

#+end_src

* Autostart
All the programs that I want to run at start.

#+begin_src sh
# Set display from arandr saved script
sh ~/.screenlayout/monitor.sh &

# Bar
~/.config/polybar/launch.sh &

# Notifications
/usr/bin/dunst &

# Polkit
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# Wallpaper
nitrogen --restore &

# Dex
dex -a -s /etc/xdg/autostart/:~/.config/autostart/

# Picom
picom -CGb &

# Network Applet
nm-applet --indicator &
#+end_src

* Xsetroot
xsetroot - root window parameter setting utility for X.

#+begin_src sh

# Cursor
xsetroot -cursor_name left_ptr &
#+end_src

* Battery notifier
Script that monitors battery.

#+begin_src sh
# Low battery notifier
~/.config/bspwm/scripts/low_bat_notifier.sh
#+end_src
