#+TITLE: xfce-terminal config
#+AUTHOR: Fet64
#+property: header-args :mkdirp yes :result silent :tangle ~/.config/xfce4/terminal/terminalrc

* Table Of Contents :toc:
- [[#about-this-config][About this config]]
- [[#configuration][Configuration]]

* About this config
XFCE-terminal, more informaion: [[https://docs.xfce.org/apps/terminal/start]]

* Configuration
#+begin_src sh

[Configuration]
BackgroundDarkness=0.860000
MiscSearchDialogOpacity=100
MiscShowUnsafePasteDialog=FALSE
BackgroundMode=TERMINAL_BACKGROUND_TRANSPARENT
MiscAlwaysShowTabs=FALSE
MiscBell=FALSE
MiscBellUrgent=FALSE
MiscBordersDefault=TRUE
MiscCursorBlinks=FALSE
MiscCursorShape=TERMINAL_CURSOR_SHAPE_BLOCK
MiscDefaultGeometry=100x30
MiscInheritGeometry=FALSE
MiscMenubarDefault=FALSE
MiscMouseAutohide=FALSE
MiscMouseWheelZoom=TRUE
MiscToolbarDefault=FALSE
MiscConfirmClose=TRUE
MiscCycleTabs=TRUE
MiscTabCloseButtons=TRUE
MiscTabCloseMiddleClick=TRUE
MiscTabPosition=GTK_POS_TOP
MiscHighlightUrls=TRUE
MiscMiddleClickOpensUri=FALSE
MiscCopyOnSelect=FALSE
MiscShowRelaunchDialog=TRUE
MiscRewrapOnResize=TRUE
MiscUseShiftArrowsToScroll=FALSE
MiscSlimTabs=TRUE
MiscNewTabAdjacent=FALSE
ColorForeground=#e3e3ea
ColorBackground=#08052b
ColorPalette=rgb(8,5,43);rgb(255,127,127);rgb(71,179,93);rgb(204,57,128);rgb(127,186,255);rgb(127,63,191);rgb(127,127,255);rgb(205,204,219);rgb(127,186,255);rgb(255,127,127);rgb(153,153,204);rgb(255,127,127);rgb(127,127,255);rgb(127,63,191);rgb(127,127,255);rgb(227,227,234)
ScrollingUnlimited=TRUE
TitleMode=TERMINAL_TITLE_REPLACE
ScrollingBar=TERMINAL_SCROLLBAR_NONE
TextBlinkMode=TERMINAL_TEXT_BLINK_MODE_FOCUSED
FontName=Source Code Pro 10
ColorCursorForeground=#FFFFFF
ColorCursor=#ff7f7f
ColorCursorUseDefault=FALSE
ColorBold=#7fbaff
ColorBoldIsBright=FALSE
TabActivityColor=#47B35D

#+end_src
